package nc.cafat.vousetesungenie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VousetesungenieApplication {

	public static void main(String[] args) {
		SpringApplication.run(VousetesungenieApplication.class, args);
	}

}
