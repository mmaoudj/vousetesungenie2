package nc.cafat.vousetesungenie.controller;

public enum Sexe {
    FEMININ,
    MASCULIN,
    AUTRE
}
