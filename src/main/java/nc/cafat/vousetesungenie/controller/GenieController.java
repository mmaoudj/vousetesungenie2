package nc.cafat.vousetesungenie.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GenieController {

    @GetMapping("/quietesvous")
    public String quiEtesVous(@RequestParam String prenom, @RequestParam Sexe sexe){
        String reponse ="Bonjour "+prenom+", vous l'ignoriez peut-être, mais on peut dire de vous que vous êtes ";
        switch (sexe) {
            case FEMININ -> reponse += "une sacrée Génie.";
            case MASCULIN -> reponse += "un sacré Génie.";
            case AUTRE -> reponse += "une drôle de chose et probablement géniale.";
        }
        return reponse;
    }
}
