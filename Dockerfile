FROM adoptopenjdk/openjdk16:ubi
COPY target/vousetesungenie-0.0.1-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "vousetesungenie-0.0.1-SNAPSHOT.jar"]